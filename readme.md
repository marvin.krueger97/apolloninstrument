This is a project created for a research course at HTW Berlin

This project aims to offer a simple way for a flexible, microtonal digital instrument which will work on mobile devices. We used a UI and visualization builded with Unity. Furthermore we evaluated different options for sound generation:
- Unity (OnAudioBufferRead)
- AudioKit for iOS
- SuperCollider 

The SuperCollider code can be found in SuperCollider folder. 

