using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneSpec : MonoBehaviour
{
    /* 
    Created by Marvin Krueger
    This file was created by us. 
     */
    public GameObject startSpec;

    private void Awake() {
        for(int i = 1; i < 128; i++){
            GameObject obj = Instantiate(startSpec);
            obj.transform.name = i.ToString();
            obj.transform.SetParent(startSpec.transform.parent, true);
        }
    }
}
