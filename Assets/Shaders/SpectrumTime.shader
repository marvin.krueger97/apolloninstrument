// Created by Marvin Krueger
// This shader is basically based on the work created here: https://www.shadertoy.com/view/4sySDt
// We converted the code into HLSL code and made some adjustments to make it suitable for our purposes
Shader "Unlit/SpectrumTime"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _spectrum ("Texture", 2D) = "white" {}
        _prevTex ("Texture", 2D) = "white" {}
        _visual ("visual", int) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            uniform int _visual; 

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex, _spectrum, _prevTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float3 B2_spline(float3 x) { // returns 3 B-spline functions of degree 2
                float3 t = 3.0 * x;
                float3 b0 = step(0.0, t)     * step(0.0, 1.0-t);
                float3 b1 = step(0.0, t-1.0) * step(0.0, 2.0-t);
                float3 b2 = step(0.0, t-2.0) * step(0.0, 3.0-t);
                return 0.5 * (
                    b0 * pow(t, float3(2.0, 2, 2)) +
                    b1 * (-2.0*pow(t, float3(2.0,2,2)) + 6.0*t - 3.0) + 
                    b2 * pow(3.0-t,float3(2.0,2,2))
                );
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // Inspiration: https://www.shadertoy.com/view/4sySDt
                if(_visual == 1){
                    float fVBars = 100.;
                    float fHSpacing = 1.00;
                    
                    
                    float x = floor(i.uv.x * fVBars)/fVBars;
                    
                    float fSample = 0;
                    float3 sampleData; 
                    if(i.uv.y < 0.01){
                        // Take actual freq
                        sampleData = tex2D(_spectrum, i.uv);    
                        if(sampleData.x > 0 || sampleData.y > 0 || sampleData.z > 0){
                            fSample = sampleData.x/2 ;
                        }
                    }else {
                        sampleData = tex2D(_spectrum, i.uv)+ tex2D(_prevTex, i.uv + float2(0, -0.01));
                        if(sampleData.x > 0 || sampleData.y > 0 || sampleData.z > 0){
                            fSample = 1;
                        }
                    }
                    float squarewave = sign(fmod(i.uv.x, 1.0/fVBars)-0.004);
                    float fft = squarewave * fSample;
                    
                    float fHBars = 100.0;
                    float fVSpacing = 0.180;
                    float fVFreq = (i.uv.y * 3.14);
                    fVFreq = sign(sin(fVFreq * fHBars)+1.0-fVSpacing);

                    float2 centered = float2(1.0,1) * i.uv - float2(1.0,1);
                    float t = _Time.y / 100.0;
                    float polychrome = 1.0;
                    float3 spline_args = frac(float3(polychrome*i.uv.x-t,polychrome*i.uv.x-t,polychrome*i.uv.x-t) + float3(0.0, -1.0/3.0, -2.0/3.0));
                    float3 spline = B2_spline(spline_args);
                    
                    float f = abs(centered.y);
                    float3 base_color  = float3(1.0, 1.0, 1.0) - f*spline;
                    float3 flame_color = pow(base_color, float3(3.0,3,3));
                    
                    float tt =i.uv.y;
                    
                    float val = 1.0 - step(fft, abs(0.3-i.uv.y));
                    float3 col = flame_color * float3(val,val,val) * float3(fVFreq,fVFreq,fVFreq);
                    
                    // output final color
                    return float4(col,1);
                } else {
                    float fVBars = 100.;
                    float fHSpacing = 1.00;
                    
                    
                    float x = floor(i.uv.x * fVBars)/fVBars;
                    //float fSample = tex2D( iChannel0, float2(abs(2.0 * x - 1.0), 0.25)).x;
                    float fSample = 0;
                    float3 sampleData = tex2D(_spectrum, float2(i.uv.x, 0.005));   
                    float uvX = i.uv.x -0.5; 
                    if(sampleData.r/1.2-abs(uvX) > 0){
                        fSample = sampleData.r/1.2-abs(uvX);
                    }
                    float squarewave = sign(fmod(i.uv.x, 1.0/fVBars)-0.004);
                    float fft = squarewave * fSample* 0.5;
                    
                    float fHBars = 100.0;
                    float fVSpacing = 0.180;
                    float fVFreq = (i.uv.y * 3.14);
                    fVFreq = sign(sin(fVFreq * fHBars)+1.0-fVSpacing);

                    float2 centered = float2(1.0,1) * i.uv - float2(1.0,1);
                    float t = _Time.y / 100.0;
                    float polychrome = 1.0;
                    float val = polychrome*i.uv.x-t;
                    float3 spline_args = frac(float3(val,val,val) + float3(0.0, -1.0/3.0, -2.0/3.0));
                    float3 spline = B2_spline(spline_args);
                    
                    float f = abs(centered.y);
                    float3 base_color  = float3(1.0, 1.0, 1.0) - f*spline;
                    float3 flame_color = pow(base_color, float3(3.0, 3,3));
                    
                    float tt = 0.3 - i.uv.y;
                    float df = sign(tt);
                    df = (df + 1.0)/0.5;
                    val = 1.0 - step(fft, abs(0.3-i.uv.y));
                    float3 col = flame_color * float3(val,val,val) * float3(fVFreq,fVFreq,fVFreq);
                    col -= col * df * 0.180;
                    
                    // output final color
                    return float4(col,1.0);
                }

    
            }
            ENDCG
        }
    }
}
