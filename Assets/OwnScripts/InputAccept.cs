﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputAccept : MonoBehaviour
{
    /* 
    Created by Marvin Krueger
    This file was created by us. It was intially developed for another project. During this project we adjust this file and used it for creating a UI / menu.
     */
    public Button acceptButton;

    public  delegate void OnClickSave(string text);
    public OnClickSave handler;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<InputField>().onValueChanged.AddListener (delegate {changed ();});
        acceptButton.interactable = false; 
    }

    public void changed(){
        if(this.GetComponent<InputField>().text != ""){
            acceptButton.interactable = true;
        } else {
            acceptButton.interactable = false;
        }
    }

    public void clickSave(){

        handler(this.GetComponent<InputField>().text);
        acceptButton.interactable = false;
        this.GetComponent<InputField>().text = "";
    }
}
