# Faust
- functional programming language 
- Audio processing, sound synthesis
- compiler backends allows to compile for many targets
    - microcontrollers
    - JUCE/VST plugin projects, 
    - Pure Data, 
    - Max and 
    - SuperCollider
    - Unity ...
    - Browser 
    - Languages: 
    - C++, C, LLVM bit code, WebAssembly, Rust
- huge library of building blocks --> Quick start is possible 
    - https://faustlibraries.grame.fr/
    - Envelops, Oscillators, physical Models .. (more sophisticated than UGens in SC sometimes, SC has no physical modelling UGens )
    - Github stars 1.5k 
    - Small community (e.g. Slack channel, Github page / project. not got an answer yet on open Github issues / Slack channel)
- Online IDE with a lot of examples 
- Supports polyphonic export (several frequencies played together)
- Easy to use interface 
    - Values are exposed, so you can e.g. change parameters of your sound synthesis model 
    - In Unity parameters are exposed as well --> Easy manipulation
    ![alt text](dspFaustExmple.png)
- Not yet sure: Can we get FFT data in Unity? Also could be possible to catch the AudioStream in Unity and do a FFT in C# --> After some research: Looks good, seems that we'll be able to compute the FFT in Unity.

- Problem so far: Does not work in Unity. Sound is not generated. Waiting for answer in Slack channel



# SuperCollider 
- platform for audiosynthesis
- library of building blocks / components (UGens)
    - filters, panning, reverbs
- More active community (Discord channels)
- Github stars 3.5k 
- Architecture: AudioServer + Interpreter language to control the server 
    - Problem: AudioServer has to run in the background
    - Currently: AudioServer has to run on the computer, potential app have to communicate through network with OSC to generate sounds on computer
