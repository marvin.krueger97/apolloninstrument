//
//  UnityPluginBridge.m
//  SCPlugin
//
//  Created by Marvin Krüger on 01.04.21.
// Idea comes from: https://medium.com/@SoCohesive/unity-how-to-build-a-bridge-ios-to-unity-with-swift-f23653f6261
//

#import <Foundation/Foundation.h>
#include "UnityFramework/UnityFramework-Swift.h"

extern "C" {
    
    #pragma mark - Functions
        
    int _addTwoNumberInIOS(int a , int b) {
    
        int result = [[UnityPlugin shared] AddTwoNumberWithA:(a) b:(b)];
        return result;
    }

    void _keyOn(float frequency, int midiNote) {
        
        [[UnityPlugin shared] keyOnFrequency:(frequency) midiNote:(midiNote)];
    }

    /*NSArray* _getFFT() {
        return [[UnityPlugin shared] getFFT];
    }*/

char* cStringCopy(const char* string) {
        char* res = (char*)malloc(strlen(string) + 1);
        strcpy(res, string);
        return res;
    }

    char* _getFFT(char* data, size_t size) {
        NSArray* arr  = [[UnityPlugin shared] getFFT];
        NSError* error = nil;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:&error];
        NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                char* s = cStringCopy([jsonString UTF8String]);
                return s;
    }



    void _setParams(int waveForm, double att, double dec, double sus, double rel){
        [[UnityPlugin shared] setParamsWithWaveForm:(waveForm) att:att dec:dec sus:sus rel:rel];
    }

    void _keyOnSimple(float midiNote) {
        [[UnityPlugin shared] keyOnSimpleWithMidiNote:(midiNote)];
    }

    void _keyOff(float midiNote){
        [[UnityPlugin shared] keyOffWithMidiNote:(midiNote)];
    }
}
