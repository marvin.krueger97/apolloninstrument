﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;
using System.IO;
public class ConfigManager : MonoBehaviour
{   
    /* 
    Created by Marvin Krueger
    This file was created by us. Intially used for another project and adjust for the purpose of this project
     */

    public JSONObject presetObj, settingsObj;

    public UIStateMachine uIStateMachineFluid;

    private void loadSettings(){
        string settings = ((TextAsset)Resources.Load("settings")).text;
        settingsObj = (JSONObject)JSON.Parse(settings);
        if(PlayerPrefs.GetString("settings") != ""){
            settingsObj = (JSONObject)JSON.Parse(PlayerPrefs.GetString("settings"));
        }
    }


    private void Awake() {
        loadSettings(); 
        
    }

    public void Start(){
        applyConfig();
        
    }

    public void applyConfig(){
        this.GetComponent<PluginHelper>().numberOfNotes = Mathf.RoundToInt(settingsObj["sections"]["Grid"]["Nr. notes"]["current"].AsFloat);
        this.GetComponent<PluginHelper>().numberOfOcatves = Mathf.RoundToInt(settingsObj["sections"]["Grid"]["Nr. octaves"]["current"].AsFloat);
        this.GetComponent<PluginHelper>().intervalMultiplier = settingsObj["sections"]["Grid"]["Octave multiplier"]["current"].AsFloat;
        this.GetComponent<PluginHelper>().visual = settingsObj["sections"]["Visuals"]["Visual"]["current"]["value"].AsInt;
        if(settingsObj["sections"]["Audio"]["Synthesis method"]["current"]["name"] == "Unity"){
            this.GetComponent<PluginHelper>().useSC = false;
            this.GetComponent<PluginHelper>().useInternalSoundGeneration = true;
        }
        if(settingsObj["sections"]["Audio"]["Synthesis method"]["current"]["name"] == "AudioKit"){
            this.GetComponent<PluginHelper>().useSC = false;
            this.GetComponent<PluginHelper>().useInternalSoundGeneration = false;
        }
        if(settingsObj["sections"]["Audio"]["Synthesis method"]["current"]["name"] == "SC"){
            this.GetComponent<PluginHelper>().useSC = true;
            this.GetComponent<PluginHelper>().useInternalSoundGeneration = false;
        }
    }

     public void saveSettings(){
         PlayerPrefs.SetString("settings", settingsObj.ToString());

         applyConfig();
     }


     public void savePresets(){
         PlayerPrefs.SetString("presets", presetObj.ToString());
         applyConfig();
     }
}