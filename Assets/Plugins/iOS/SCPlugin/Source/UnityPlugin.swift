import Foundation
import AudioKit

@objc public class UnityPlugin : NSObject {
    @objc public static var oscillator = AKOscillatorBank()
    @objc public static let shared = UnityPlugin()
    @objc public static var fft: AKFFTTap?
    
    
    @objc public func AddTwoNumber(a:Int,b:Int ) -> Int {
        
    let square = AKTable(.square, count: 256)
    let triangle = AKTable(.triangle, count: 256)
    let sine = AKTable(.sine, count: 256)
    let sawtooth = AKTable(.sawtooth, count: 256)
   
    
        print("Init");
        
        //UnityPlugin.fft = AKFFTTap.init(UnityPlugin.oscillator)
        UnityPlugin.oscillator.attackDuration = 0.25
        UnityPlugin.oscillator.decayDuration = 0.35
        UnityPlugin.oscillator.sustainLevel = 0
        UnityPlugin.oscillator.releaseDuration = 0.65
        UnityPlugin.oscillator = AKOscillatorBank(waveform: sine)
        AKManager.output = UnityPlugin.oscillator
        
        
     do {
        try AKManager.start()
    } catch  {
    
    }
        UnityPlugin.fft = AKFFTTap.init(UnityPlugin.oscillator)
        return a + b
}
    @objc public func setParams(waveForm: UInt8, att: Double, dec: Double, sus: Double, rel: Double){
        let square = AKTable(.square, count: 256)
        let triangle = AKTable(.triangle, count: 256)
        let sine = AKTable(.sine, count: 256)
        let sawtooth = AKTable(.sawtooth, count: 256)
        
        UnityPlugin.oscillator.attackDuration = att
        UnityPlugin.oscillator.decayDuration = dec
        UnityPlugin.oscillator.sustainLevel = sus
        UnityPlugin.oscillator.releaseDuration = rel
    }
    

    @objc public func keyOn(frequency: Float, midiNote: Int){
        UnityPlugin.oscillator.attackDuration = 0.15
        UnityPlugin.oscillator.decayDuration = 0.15
        UnityPlugin.oscillator.sustainLevel = 0.2
        UnityPlugin.oscillator.releaseDuration = 0.15
        UnityPlugin.oscillator.play(noteNumber: UInt8.init(1), velocity: 127, frequency: Double(frequency))
        print("Play ... ");
        print(String(frequency));
    }
    
    @objc public func keyOnSimple(midiNote: Float){
        /*UnityPlugin.oscillator.attackDuration = att
        UnityPlugin.oscillator.decayDuration = dec
        UnityPlugin.oscillator.sustainLevel = sus
        UnityPlugin.oscillator.releaseDuration = rel*/
        //UnityPlugin.oscillator.play(noteNumber:  midiNote, velocity: 127)
        
        /*UnityPlugin.oscillator.frequency = Double(midiNote);
        UnityPlugin.oscillator.start();*/
    
}
   /* @objc public  func getFFT() -> [Double]{
        return UnityPlugin.fft!.fftData
    
}*/
 @objc public  func getFFT() -> [Double]{
        return UnityPlugin.fft!.fftData
    
}   

    @objc public  func keyOff(midiNote: Int){
    
    UnityPlugin.oscillator.stop(noteNumber:  UInt8(midiNote))
    
}
}
