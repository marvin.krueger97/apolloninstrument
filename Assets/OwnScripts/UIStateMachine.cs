﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class UIStateMachine : MonoBehaviour
{
    /* 
    Created by Marvin Krueger
    This file was created by us. It was intially developed for another project. During this project we adjust this file and used it for creating a UI / menu.
     */
    public ConfigManager Settings;
    public GameObject sliderObj, headingObj, checkObj, dropdownObj, inputObj, btnObj, quitObject;

    
    
    public GameObject contentWrapper;



     public GameObject menuWindow, baseUI;
     public GameObject scrollView;


    public Text menuText;

    //public Purchaser purchaser;
    void Start()
    {

        if(PlayerPrefs.GetInt("onloadShowGUI") == 1){
            PlayerPrefs.SetInt("onloadShowGUI", 0);
            baseUI.GetComponent<UIController>().Show();
        }

        float windowHeight = Screen.height / this.transform.localScale.x + 30f;
        menuWindow.GetComponent<RectTransform>().sizeDelta = new Vector2(menuWindow.GetComponent<RectTransform>().sizeDelta.x, windowHeight);
        Vector3 oldPos = menuWindow.transform.GetChild(0).GetComponent<RectTransform>().localPosition; 
        menuWindow.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(menuWindow.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x, windowHeight);
        menuWindow.transform.GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(oldPos.x, 0, oldPos.z);

        scrollView.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollView.GetComponent<RectTransform>().sizeDelta.x, windowHeight *0.8f);
        Vector3 oldScrollPos = scrollView.GetComponent<RectTransform>().localPosition;
        scrollView.GetComponent<RectTransform>().localPosition = new Vector3(oldScrollPos.x,  -25f, oldScrollPos.z);
        
        createMenuItems();


         
    }

    public void showMenuWindow(){
        menuText.text = "Menu";
        createMenuItems(); 
        menuWindow.GetComponent<UIController>().Show(); 
    }

    public void createMenuItems(){
        sliderObj.SetActive(true);
        headingObj.SetActive(true);
        checkObj.SetActive(true);
        dropdownObj.SetActive(true);
        inputObj.SetActive(true);
        btnObj.SetActive(true);
        quitObject.SetActive(true);

        foreach (Transform child in contentWrapper.transform) {
            GameObject.Destroy(child.gameObject);
        }
        GameObject menuWindow = GameObject.Find("MenuWindow");
        GameObject sectionGO = null;
        menuWindow = contentWrapper;

        foreach (KeyValuePair<string, JSONNode> N in Settings.settingsObj["sections"]){
            sectionGO = new GameObject();
            sectionGO.name = N.Key; 
            sectionGO.transform.parent = menuWindow.transform;
            sectionGO.transform.localScale = Vector3.one;
            createSection(Settings.settingsObj["sections"][N.Key], sectionGO, N.Key);
        }
        sliderObj.SetActive(false);
        btnObj.SetActive(false);
        headingObj.SetActive(false);
        checkObj.SetActive(false);
        dropdownObj.SetActive(false);
        inputObj.SetActive(false);
        quitObject.SetActive(false);

    }

    public void quit(){
        Application.Quit();
    }

    public void createSection(JSONNode section, GameObject sectionGO, string sectionName, bool isModeSection = false){
        
            createHeadline(sectionName, sectionGO);
        

            foreach (KeyValuePair<string, JSONNode> N in Settings.settingsObj["sections"][sectionName]){
                if(Settings.settingsObj["sections"][sectionName][N.Key]["type"] == "float"){
                    createParameterSectionSlider(Settings.settingsObj["sections"][sectionName][N.Key], sectionGO, N.Key, sectionName);
                    
                }
                if(Settings.settingsObj["sections"][sectionName][N.Key]["type"] == "bool"){
                    createParameterSectionCheckbox(Settings.settingsObj["sections"][sectionName][N.Key], sectionGO, N.Key, sectionName);
                }
                if(Settings.settingsObj["sections"][sectionName][N.Key]["type"] == "button"){
                    createButtonSection(sectionGO, N.Key, sectionName, Settings.settingsObj["sections"][sectionName][N.Key]["action"]);
                }
                if(Settings.settingsObj["sections"][sectionName][N.Key]["type"] == "list"){
                    createParameterSectionDropdown(Settings.settingsObj["sections"][sectionName][N.Key], sectionGO, N.Key, sectionName);
                }
                if(Settings.settingsObj["sections"][sectionName][N.Key]["type"] == "string"){
                    createParameterInput(sectionGO, N.Key, Settings.settingsObj["sections"][sectionName][N.Key]["current"]);
                }

            }
       
        

    }

    public void createParameterInput(GameObject sectionGO, string name, string value){
        GameObject newInput = Instantiate(inputObj);
        newInput.transform.SetParent(contentWrapper.transform, false);
        //newInput.transform.localScale = inputObj.transform.localScale;
        newInput.GetComponent<SectionName>().sectionName = sectionGO.name;
        newInput.transform.Find("ParamName").GetComponent<Text>().text = name; 
        newInput.transform.Find("InputField").GetComponent<InputField>().SetTextWithoutNotify(value);
        InputAccept input = newInput.transform.Find("InputField").GetComponent<InputAccept>();
        
        input.handler = inputString;

    }

    public void inputString(string presetName){
        Debug.Log("Enetered" + presetName);
        Settings.settingsObj["sections"]["SuperCollider"]["IP"]["current"] = presetName;
        Settings.saveSettings(); 
    }

    private void createHeadline(string name, GameObject sectionGO){
        GameObject newHeading = Instantiate(headingObj);
        newHeading.transform.SetParent(contentWrapper.transform, false);
        newHeading.GetComponent<Text>().text = name;
    }

     public void createQuit(){
        GameObject contact = Instantiate(quitObject);
        contact.transform.SetParent(contentWrapper.transform, false);
    }

    private bool showItem(JSONNode section, JSONNode param){
        for(int i = 1; i <= 5; i++){
            string key = "showIfValue"; 
            if(i>1){
            key += i.ToString();

            }

            if(param[key] != null){
                if(section[param["showIfName"].ToString().Replace("\"", "")]["current"]["value"] == param[key]){
                    if(param["showIfSecondName"] == null){
                        return true;
                    } else {
                        return section[param["showIfSecondName"].ToString().Replace("\"", "")]["current"]["value"] == param["showIfSecondValue"];
                    }
                }
                try
                {
                    if(section[param["showIfName"].ToString().Replace("\"", "")]["current"].Equals(param[key]) ||
                    section[param["showIfName"].ToString().Replace("\"", "")]["current"].ToString() == param[key].ToString()){
                        if(param["showIfSecondName"] == null){
                            return true;
                        } else {
                            return section[param["showIfSecondName"].ToString().Replace("\"", "")]["current"].Equals(param["showIfSecondValue"]) ||
                    section[param["showIfSecondName"].ToString().Replace("\"", "")]["current"].ToString() == param["showIfSecondValue"].ToString();
                        }
                    }
                }
                catch (System.Exception)
                {
                }
            }

        }
            
            return false;
    }

    public void createParameterSectionSlider(JSONNode param, GameObject sectionGO, string paramName, string sectionName, bool isModeSection = false, GameObject parent = null){
        if(
            (isModeSection && (!param["showIfName"].IsString || (showItem(Settings.settingsObj["Modes"][sectionName]["params"], param)))) ||
            (!isModeSection && (!param["showIfName"].IsString || (showItem(Settings.settingsObj["sections"][sectionName], param))))
            ){
            GameObject newSlider = Instantiate(sliderObj);
            if(parent == null){
                newSlider.transform.SetParent(contentWrapper.transform, false);
            } else {
                newSlider.transform.SetParent(parent.transform, false);
                GameObject go = new GameObject(); 
                go.transform.parent = parent.transform;
            }
            newSlider.GetComponent<SectionName>().sectionName = sectionGO.name;
            newSlider.transform.Find("ParamName").GetComponent<Text>().text = paramName; 
            if(paramName.Contains("sensitivity")){
                newSlider.transform.Find("ParamName").GetComponent<Text>().fontSize = 60; 
            }
            newSlider.transform.Find("ParamValue").GetComponent<Text>().text = (Mathf.Round(param["current"]*100f)/100f).ToString();
            Slider slider = newSlider.transform.Find("Slider").GetComponent<Slider>();
            slider.value = translateRange(param["from"], param["to"], 0, 1, param["current"]);
            slider.onValueChanged.AddListener (delegate {sliderValueChanged (slider);});

        }
    }
     public void createParameterSectionDropdown(JSONNode param, GameObject sectionGO, string paramName, string sectionName, bool isModeSection = false){
        if(
            (isModeSection && (!param["showIfName"].IsString || (showItem(Settings.settingsObj["Modes"][sectionName]["params"], param)))) ||
            (!isModeSection && (!param["showIfName"].IsString || (showItem(Settings.settingsObj["sections"][sectionName], param))))
            ){
            GameObject newDropdown = Instantiate(dropdownObj);
            newDropdown.transform.SetParent(contentWrapper.transform, false);
            //newDropdown.transform.localScale = sliderObj.transform.localScale;
            newDropdown.GetComponent<SectionName>().sectionName = sectionGO.name;
            newDropdown.transform.Find("ParamName").GetComponent<Text>().text = paramName; 
            Dropdown dropdown = newDropdown.transform.Find("Dropdown").GetComponent<Dropdown>();
            dropdown.options.Clear();
            int dropdownValue = 0;
            List<string> plusSignLabels = new List<string>();
            foreach (JSONNode item in param["all"].AsArray)
            {   
                
                Dropdown.OptionData option = new Dropdown.OptionData() {text=item["name"].ToString().Replace("\"", "")};
                dropdown.options.Add (option);
                if(item["name"] == param["current"]["name"]){
                    dropdownValue = dropdown.options.Count-1;
                }
            }
            dropdown.value = dropdownValue;
            
            dropdown.onValueChanged.AddListener (delegate {dropdownValueChanged (dropdown);});
        }
    }

    public void createButtonSection(GameObject sectionGO, string paramName, string sectionName, string btnName, bool isModeSection = false){
        if(!isModeSection){
            if(!Settings.settingsObj["sections"][sectionName][paramName]["showIfName"].IsString 
            || (
                showItem(Settings.settingsObj["sections"][sectionName], Settings.settingsObj["sections"][sectionName][paramName])
                )){
                GameObject newButton = Instantiate(btnObj);
                newButton.transform.SetParent(contentWrapper.transform, false);
                newButton.GetComponent<SectionName>().sectionName = sectionGO.name;
                newButton.transform.Find("ParamName").GetComponent<Text>().text = paramName; 
                UnityEngine.UI.Button btn = newButton.transform.Find("Ok").GetComponent<UnityEngine.UI.Button>();
                btn.transform.GetChild(0).GetComponent<Text>().text = btnName;
                btn.onClick.AddListener (delegate {buttonValueChanged (btn);});
            }
        } else {
            Debug.Log("Button creation problem :O");
        }
    }
    public void createParameterSectionCheckbox(JSONNode param, GameObject sectionGO, string paramName, string sectionName, bool isModeSection = false){
        if(
            (isModeSection && (!param["showIfName"].IsString || (showItem(Settings.settingsObj["Modes"][sectionName]["params"], param)))) ||
            (!isModeSection && (!param["showIfName"].IsString || (showItem(Settings.settingsObj["sections"][sectionName], param))))
            ){
                bool allowed = true; 
                #if UNITY_ANDROID
                if(paramName == "Vibrating" || paramName == "Gyro Wall" || paramName == "Gyro Part." || paramName == "Gyro Space" || paramName == "Gyro Swarm"){
                    allowed = false;
                }
                #elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
                if(paramName == "Vibrating" || paramName == "Gyro Wall" || paramName == "Gyro Part." || paramName == "Gyro Space" || paramName == "Gyro Swarm"){
                    allowed = false;
                }
                #endif 

                if(allowed){
                    GameObject newToggle = Instantiate(checkObj);
                    newToggle.transform.SetParent(contentWrapper.transform, false);
                    newToggle.GetComponent<SectionName>().sectionName = sectionGO.name;
                    newToggle.transform.Find("ParamName").GetComponent<Text>().text = paramName; 
                    UnityEngine.UI.Toggle toggle = newToggle.transform.Find("Toggle").GetComponent<UnityEngine.UI.Toggle>();
                    toggle.isOn = param["current"];
                    toggle.onValueChanged.AddListener (delegate {toggleValueChanged (toggle);});
                }
        }
    }

    private float translateRange(float oldMin, float oldMax, float newMin, float newMax, float oldValue){
        float oldRange = (oldMax - oldMin);
        float newRange = (newMax - newMin); 
        return (((oldValue - oldMin) * newRange) / oldRange) + newMin;
    }

    private void sliderValueChanged(Slider slider){
        string changedParamName = slider.transform.parent.Find("ParamName").GetComponent<Text>().text;
        string sectionName = slider.transform.parent.GetComponent<SectionName>().sectionName;

            Settings.settingsObj["sections"][sectionName][changedParamName]["current"] = translateRange(0, 1, Settings.settingsObj["sections"][sectionName][changedParamName]["from"], Settings.settingsObj["sections"][sectionName][changedParamName]["to"], slider.value);

            if(changedParamName == "Angle" && sectionName == "Mandala"){

                // DO something
            }

        
        saveAndApplyNeededSettings(sectionName);
        
            slider.transform.parent.Find("ParamValue").GetComponent<Text>().text = (Mathf.Round(Settings.settingsObj["sections"][sectionName][changedParamName]["current"]*100f)/100f).ToString();

        
    }

    public void saveAndApplyNeededSettings(string sectionName){
        bool applyBackground = sectionName == "Background"; 
        bool applyPostProcessing = sectionName == "Effects";
        bool applyAutopilot = sectionName == "Randomize";
        bool applyMode = sectionName == "Color" || sectionName == "Mode";
        Settings.saveSettings();
    }


    private void toggleValueChanged(UnityEngine.UI.Toggle toggle){
        string changedParamName = toggle.transform.parent.Find("ParamName").GetComponent<Text>().text;
        string sectionName = toggle.transform.parent.GetComponent<SectionName>().sectionName;
            Settings.settingsObj["sections"][sectionName][changedParamName]["current"] = toggle.isOn;
        
        if(changedParamName == "Enabled" && sectionName == "Texture"){
            // Do something
        } 
        saveAndApplyNeededSettings(sectionName);
        
        createMenuItems();
    }

    private void buttonValueChanged(UnityEngine.UI.Button button){
        string action = button.transform.GetChild(0).GetComponent<Text>().text;
        if(action == "Scaling"){

           Camera.main.GetComponent<PluginHelper>().toggleScalingMode();
        } else if(action == "SC"){
            // Connect with SuperCollider
            Camera.main.GetComponent<PluginHelper>().connectToSC();
        }

        createMenuItems();
    }



    private void dropdownValueChanged(Dropdown dropdown){
        string changedParamName = dropdown.transform.parent.Find("ParamName").GetComponent<Text>().text;
        string sectionName = dropdown.transform.parent.GetComponent<SectionName>().sectionName;
        
        
            Settings.settingsObj["sections"][sectionName][changedParamName]["current"]["name"] = dropdown.options[dropdown.value].text;
            setValue(Settings.settingsObj["sections"][sectionName][changedParamName], dropdown.options[dropdown.value].text);

            if(changedParamName == "Mapping"){
                
            }

        saveAndApplyNeededSettings(sectionName);
        createMenuItems();
    }

    private  void setValue(JSONNode param, string name, bool isPreset = false){
        foreach (JSONNode item in param["all"])
        {
            if(item["name"].ToString().Replace("\"", "") == name){
                if(isPreset){
                    param["current"]["value"] = item["value"].Clone();
                } else {
                    param["current"]["value"] = item["value"];
                }
            }
        }
    }
}
