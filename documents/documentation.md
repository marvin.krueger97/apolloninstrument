%Dokumenteinstellungen und Anpassungen
%Dokumentenklasse "scrbook" - Erweitert um den Verweis auf die Verzeichnisse und Texteigenschaften
\documentclass[chapterprefix=true, 12pt, a4paper, oneside, parskip=half, listof=totoc, bibliography=totoc, numbers=noendperiod]{scrbook}

\usepackage{filecontents}
\begin{filecontents}{bib}

@online{milkdropPop,
	author  = {Dirk Netter},
	title   = {Milkdrop Psychedelische Musikvisualisierung},
	urlseen = {2021-08-04},
	url     = {https://lucys-magazin.com/milkdrop-psychedelische-musikvisualisierung/},
	keywords = {online}
}


@online{jBergButterChurn,
	author  = {Jordan Berg},
	title   = {ButterChurn},
	urlseen = {2021-08-04},
	url     = {https://github.com/jberg/butterchurn},
	keywords = {online}
}

@online{milkdropConv,
	author  = {Jordan Berg},
	title   = {ButterChurn converter},
	urlseen = {2021-08-04},
	url     = {https://github.com/jberg/milkdrop-preset-converter-node},
	keywords = {online}
}




@online{customWaveform,
	author  = {Jordan Berg},
	title   = {ButterChurn Custom Waveforms},
	urlseen = {2021-08-04},
	url     = {https://github.com/jberg/butterchurn/blob/master/src/rendering/waves/customWaveform.js},
	keywords = {online}
}


@online{commonEngines,
	author  = {Marcus Toftedahl},
	title   = {Which are the most commonly used Game Engines?},
	urlseen = {2021-08-04},
	url     = {https://www.gamasutra.com/blogs/MarcusToftedahl/20190930/350830/Which_are_the_most_commonly_used_Game_Engines.php},
	keywords = {online}
}

@online{musicTheory1,
	author  = {The Editors of Encyclopaedia Britannica},
	title   = {Octave | Music theory},
	urlseen = {2021-08-04},
	url     = {https://www.britannica.com/art/octave-music},
	keywords = {online}
}

@online{drawModes,
	author  = {Tutorialspoint},
	title   = {WebGL - Modes of drawing},
	urlseen = {2021-08-04},
	url     = {https://www.tutorialspoint.com/webgl/webgl_modes_of_drawing.htm},
	keywords = {online}
}

@article{unityCite,
author = {Christopoulou, Eleftheria and Xinogalos, Stelios},
year = {2017},
month = {12},
pages = {21-36},
title = {Overview and Comparative Analysis of Game Engines for Desktop and Mobile Devices},
volume = {4},
journal = {International Journal of Serious Games},
doi = {10.17083/ijsg.v4i4.194}
}

@online{shaderSource,
	author  = {Mozilla},
	title   = {WebGLRenderingContext.shaderSource()},
	urlseen = {2021-08-04},
	url     = {https://developer.mozilla.org/en-US/docs/Web/API/WebGLRenderingContext/shaderSource},
	keywords = {online}
}

@online{evalJS,
	author  = {Dr. Axel Rauschmayer},
	title   = {Evaluating JavaScript code via eval() and new Function()},
	urlseen = {2021-08-04},
	url     = {https://2ality.com/2014/01/eval.html},
	keywords = {online}
}
@online{spectrumUnity,
	author  = {Unity},
	title   = {AudioSource.GetSpectrumData},
	urlseen = {2021-08-04},
	url     = {https://docs.unity3d.com/ScriptReference/AudioSource.GetSpectrumData.html},
	keywords = {online}
}

@online{unityData,
	author  = {Unity},
	title   = {AudioSource.GetData},
	urlseen = {2021-08-04},
	url     = {https://docs.unity3d.com/ScriptReference/AudioClip.GetData.html},
	keywords = {online}
}

@online{unityButterchurn,
	author  = {Marvin Krüger},
	title   = {ButterChurn and Unity implementation comparison},
	urlseen = {2021-08-18},
	url     = {https://youtu.be/7iGOGqW1Bu8},
	keywords = {online}
}

@online{VythmJR,
	author  = {Marvin Krüger},
	title   = {Vythm JR Butterchurn visulizations},
	urlseen = {2021-08-18},
	url     = {https://www.youtube.com/watch?v=muRGUffgIqQ},
	keywords = {online}
}

@online{audiokit,
	author  = {AudioKit},
	title   = {AudioKit Github},
	urlseen = {2021-10-04},
	url     = {https://github.com/AudioKit/AudioKit},
	keywords = {online}
}

@online{milkdropPres,
	author  = {Fumbling\_Foo},
	title   = {Milkdrop presets release},
	urlseen = {2021-08-18},
	url     = {http://forums.winamp.com/showthread.php?t=396662},
	keywords = {online}
}

@online{kammerton,
	author  = {MIDI note numbers and center frequencies},
	title   = {Inspired Acoustics (IA)},
	urlseen = {2021-10-04},
	url     = {https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies},
	keywords = {online}
}



@online{frequencyTime,
	author  = {sharetechnote},
	title   = {RF - Time Domain vs Frequency Domain },
	urlseen = {2021-08-04},
	url     = {https://www.sharetechnote.com/html/RF_Handbook_TimeDomain_FrequencyDomain.html},
	keywords = {image}
}


@online{bridging,
	author  = {Sonam},
	title   = {Unity — How to Build a Bridge: iOS to Unity with Swift },
	urlseen = {2021-08-04},
	url     = {https://medium.com/@SoCohesive/unity-how-to-build-a-bridge-ios-to-unity-with-swift-f23653f6261},
}


@online{geoshred,
	author  = {Wizdom Music LLC},
	title   = {Geoshred },
	urlseen = {2021-10-04},
	url     = {https://apps.apple.com/us/app/geoshred/id1064769019},
}
@online{podfile,
	author  = {Cocoapods},
	title   = {Cocoapods },
	urlseen = {2021-10-04},
	url     = {https://guides.cocoapods.org/using/the-podfile.html},
}



@online{synthOne,
	author  = {AudioKit Pro},
	title   = {Synth One },
	urlseen = {2021-10-04},
	url     = {https://apps.apple.com/us/app/audiokit-synth-one-synthesizer/id1371050497},
}

@online{unityWebsite,
	author  = {Unity Technologies},
	title   = {Unity },
	urlseen = {2021-10-04},
	url     = {https://unity3d.com/},
}

@online{noteFreq,
	author  = {Charles Wenzel},
	title   = {Musical note frequencies },
	urlseen = {2021-10-04},
	url     = { http://techlib.com/reference/musical_note_frequencies.htm},
}





@online{oscRef,
	author  = {Matt},
	title   = {OSC Introduction },
	urlseen = {2021-10-04},
	url     = {http://opensoundcontrol.org/spec-1_0.html#introduction },
}

@online{oscJack,
	author  = {keijiro},
	title   = {OSCJack },
	urlseen = {2021-10-04},
	url     = {https://github.com/keijiro/OscJack },
}



@online{microArticle,
	author  = {Aikin, Jim},
	title   = {Alternative tuning in electronical music },
	urlseen = {2021-10-04},
	url     = {https://web.archive.org/web/20080515230851/http://emusician.com/tutorials/emusic_playing_cracks/},
}

@online{oscPattern,
	author  = {McGill University},
	title   = {Open Sound Control (OSC) },
	urlseen = {2021-10-04},
	url     = {https://www.music.mcgill.ca/~gary/306/week9/osc.html},
}



 
@ARTICLE{fft,
  author={Pielemeier, W.J. and Wakefield, G.H. and Simoni, M.H.},
  journal={Proceedings of the IEEE}, 
  title={Time-frequency analysis of musical signals}, 
  year={1996},
  volume={84},
  number={9},
  pages={1216-1230},
  doi={10.1109/5.535242}}
  
  
  % https://www.ee.columbia.edu/~dpwe/papers/PielWS96-mustf.pdf
  
  @ARTICLE{vu,
  author={Khan Vu Nguyen},
  journal={Masterarbeit HTW Berlin}, 
  title={Real time integration of ambisonics and 3D graphics into SuperCollider}, 
  year={2021}}


\end{filecontents}

\usepackage[backend=biber,style=numeric,sorting=none]{biblatex}
\addbibresource{bib}


%Anpassung der Seitenränder (Standard bottom ca. 52mm anbzüglich von ca. 4mm für die nach oben rechts gewanderte Seitenzahl)
\usepackage[bottom=48mm,left=25mm,right=25mm]{geometry}

%Tweaks für scrbook
\usepackage{scrhack}

\usepackage{textcmds}

%Blindtext
\usepackage{blindtext}

%Erlaubt unteranderem Umbrücke captions
\usepackage{caption}

%Stichwortverzeichnis
\usepackage{imakeidx}

%Kompakte Listen
\usepackage{paralist}

%Zitate besser formatieren und darstellen
\usepackage{epigraph}

%Glossar, Stichworverzeichnis (Akronyme werden als eigene Liste aufgeführt)
\usepackage[toc, acronym]{glossaries} 

%Anpassung von Kopf- und Fußzeile
%beinflusst die erste Seite des Kapitels
\usepackage[automark,headsepline]{scrlayer-scrpage}
\input{resources/styles/header_footer}

%Auskommentieren für die Verkleinerung des vertikalen Abstandes eines neuen Kapitels
%\renewcommand*{\chapterheadstartvskip}{\vspace*{.25\baselineskip}}

%Zeilenabstand 1,5
\usepackage[onehalfspacing]{setspace}

%Verbesserte Darstellung der Buchstaben zueinander
\usepackage[stretch=10]{microtype}

%Deutsche Bezeichnungen für angezeigte Namen (z.B. Innhaltsverzeichnis etc.)
\usepackage[english]{babel}

%Unterstützung von Umlauten und anderen Sonderzeichen (UTF-8)
\usepackage{lmodern}
\usepackage[utf8]{luainputenc}
\usepackage[T1]{fontenc}

%Einfachere Zitate
\usepackage{epigraph}

%Unterstützung der H positionierung (keine automatische Verschiebung eingefügter Elemente)
\usepackage{float} 

%Erlaubt Umbrüche innerhalb von Tabellen
\usepackage{tabularx}

%Erlaubt Seitenumbrüche innerhalb von Tabellen
\usepackage{longtable}

%Erlaubt die Darstellung von Sourcecode mit Highlighting
\usepackage{listings}

%Definierung eigener Farben bei nutzung eines selbst vergebene Namens
\usepackage[table,xcdraw]{xcolor}

%Vektorgrafiken
\usepackage{tikz}

%Grafiken (wie jpg, png, etc.)
\usepackage{graphicx}

%Grafiken von Text umlaufen lassen
\usepackage{wrapfig}

%Ermöglicht Verknüpfungen innerhalb des Dokumentes (e.g. for PDF), Links werden durch "hidelink" nicht explizit hervorgehoben
\usepackage[hidelinks,german]{hyperref}

%Einbindung und Verwaltung von Literaturverzeichnissen
\usepackage{csquotes} %wird von biber benötigt

\input{resources/styles/adjustments}

%Titelformen - gewünschtes Layout einkommentieren

%%Graduation
\include{titles/graduation}
\gradeType{Master of Science (M.Sc.)}
\secondExaminer{Max Mustermann}

%Research paper
%\include{titles/research_papger}
%\subTitle{Ein optionaler Untertitel der Arbeit}
%\researchPart{A}

%Angaben zur Arbeit und dem Author (von beiden Layouts genutzt)
\title{Sound-synthesis and visualizations in realtime }
\author{Marvin Krüger}
\matrikelnr{s0556109}
\submitDate{SS 21}
\firstExaminer{Marvin Krüger}

%Verzeichnisse generieren
\makeglossaries
\loadglsentries{references/glossary_acronyms.tex}
\setacronymstyle{long-short}

\makeindex[columns=2, title=Stichwortverzeichnis, options= -s resources/styles/indexstyle.ist, intoc]
\indexsetup{level=\chapter*,toclevel=chapter}

%Start des Inhalts
\begin{document}

%Notwendiger Workaround
\pagenumbering{alph}

%Deckblatt erzeugen
\maketitle

\pagenumbering{Roman} 

%Inhaltsverzeichnis
\tableofcontents \newpage

%Hauptteil
\pagenumbering{arabic}

\chapter{Motivation} \label{c:beispiele}
While the computational power increased drastically over the last decades and powerful mobile devices were developed, a lot of digital instruments are available to make it easy and intiuive for musicians to create musical pieces even on a small device like a smartphone or tablet \cite{geoshred} \cite{synthOne}.
In western music we often use a tonal system which divides sounds into octaves. Each octave is hereby separated into 12 notes. Whenever a note is played and sound is generated, a specific mixture of sound waves with a certain pitch frequency are producing the hearable sound character. The 12 note system leads to some limitations when creating music. To overcome this issue, alternative systems were developed which are in general called microtonal \cite{microArticle}.
When composing music, some helpful concepts like scales are used to help creating sequences of notes which sound harmonic together and trigger certain emotions like sadness or happiness. Also a frequency spectrum can help a musician to better compose music and give the current piece a certain, visually noteable structure. 
Our goal was to create a digital instrument we can use on mobile devices which gives the user flexibility and offers some tools to make it intuitive to create music. This project aims to overcome the limitations of a 12 note system and provide a way for the user to use a microtonal system in a flexible way. Also it was important for us to add an option for so called pitch bending, so the user can create an effect similar to a vibrato on a guitar when playing a certain note. Furthermore we wanted to display a frequency spectrum over time which can help to give a better understanding of the structure of the currently played music. 


\chapter{Basic concepts}
At first it's important to introduce some theoretical concepts which were required to create the project.

\section{Frequency spectrum}

In signal theory we distinguish between the time-domain and the frequency-domain data. When an audio file is streamed to the speakers, it's divided into samples. We use the samplerate measured in Hz to define how many samples will be defined per second. When using this samples, we have time-based data (time-domain). We know about one signal for each point in time. On the other hand the speakers produce several sound waves based on the delivered samples and when we hear a sound using our ears, it's defined by a bunch of simultanously played soundwaves with different wavelength (frequencies / frequency-domain). The image \ref{fig2} shows some example data for both, frequency- and time-domain. When hearing this sounds with our ears, we can divide soundwaves with different frequencies. When a wave has a low frequency, we can hear it as a low tone. Very short waves with a high frequency  can be interpreted as high tones. Sometimes it's required to transform time-domain based samples into frequency-domain based data. For this purpose the FFT (Fast Fourier Transformation) is often used. The algorithm takes a bunch of samples as an input (for example 1024) and outputs the currently played frequencies \cite{fft}.


\begin{figure}[ht]
	\centering
  \includegraphics[width=0.9\textwidth]{frequencyTime.png}
	\caption{Time- and frequency-domain\cite{frequencyTime}}
	\label{fig2}
\end{figure}

\section{How to calculate frequencies for notes | Microtonal frequencies}
In western music, we often divide an ocatve into 12 notes. Where an Octave is an interval where the lower notes has half of the frequency of it's next, higher note \cite{musicTheory1}.
Because we have a factor two between two notes of an octave and we have 12 notes per octave, each note is separated by the factor: \[ 2^{1/12} = 1.059463 \]  to the next higher note. The factor 2 is used because of the interval between two octaves. The 12 in the exponent is used because we have exactly 12 notes per octave. \n
When we want to calculate the frequency of a note, we can use another note with a certain frequency as a starting point. Then we need to now how many notes away our other note is. Given this, we can calculate the notes frequency by using: \[ Freq = note * 2 ^{N/12} \]
Where N is the distance between our starting note and the other note \cite{noteFreq}.\n

In music theory, the so called Kammerton with 440Hz is often used as a starting point, we also use this note as a base note \cite{kammerton}.

Now when we put that together, we can come up with a more flexible formula to divide our octave into more or less than 12 notes. Aditionally, we can use so called pseudo-octaves instead of octaves, so the interval between two notes in an octave does not differ by factor 2 but by another factor. When we call our octave multiplier o and the number of notes per octave p, we can calculate a certain note frequency by using this formula:  \[ Freq = note * o^{N/p} \]

Two illustrate this properly we want to give one example here. When we want to use the pseudo-octave multiplier 2.71 and divide one pseudo-octave into 14 notes, we can determine the next higher notes frequency by calculating this: \[ Freq = 440*2.71^{1/14}\] \[ Freq = 472.5 \]

\section{OSC}
``Open Sound Control (OSC) is an open, transport-independent, message-based protocol developed for communication among computers, sound synthesizers, and other multimedia devices'' \cite{oscRef}.\n

It allows to send and receive messages to pre-defined endpoints. The OSC protocol allows to send different kind of datatypes, for example integer, float and string values\cite{oscRef}. \n 

The general format for an OSC message follows this pattern: \n

``<address pattern>   <type tag string>   <arguments>'' \n 

Where the address begins with a slash. Then we define the type of the variables we want to send and finally we specify the variable values / arguments \cite{oscPattern}.



\chapter{Choosing the right tools}
As we want to create an digital instrument, what we need are bascially two things. First is a graphical interface for user interaction and visual feedback (frequency spectrum and possible other visualizations). The second is a tool for real-time audio synthesis which allows us to dynamically create the necessary sounds which are requested by the user. 

\section{Graphical interface}
As we want to create graphical elements and use the system even on a mobile device, the game engine Unity seems to be a good choice as it is used widely for graphical applications, also on mobile devices like Android and iOS. Unity ... ``is one of the best-known game engines. Unity provides advanced 3D
graphics rendering and exports to mobile devices for free. Still, it is one of the most popular
engines in the gaming industry, with which many successful games have been created, such as
Deus Ex: The Fall, for mobile phones, or Assassin's Creed: Identity'' \cite{unityCite}.  \n 

It has the ability to create 2d and 3d graphics as well as user interfaces and has option to export the project for mobile devices or computers \cite{unityWebsite}.

\section{Sound synthesis}
In this project we begun to evaluate three different options for sounds synthesis. We tried to create sounds in realtime using Unity itself, using AudioKit for iOS and also evaluated SuperCollider for sound generation. 

\chapter{Sound generation}
\section{In Unity}
Unity gives us an option to manipulate the audio buffer before the data is played back through the speakers. For this Unity has a callback method OnAudioFilterRead. When implementing this method we can manipulate an array of floats where each float value stands for one sample in the time domain of our audio signal. We generated a sine waveform for each frequency which should be played and added them together for each sample / data point in the audio buffer array. \n 

The code we used to generate the sound can be found in figure \ref{unityFreq}. In the OnAudioFilterRead method we have a nested loop. The outer loop go through all frequencies which should be played. The inner loop iterate over each audio buffer sample. Inside the loops we calculate a value called incrementAmount by using the actual frequency plus a vibrato value to slightly change the frequency. As we move forward in time, we add this incrementAmount to a variable called phase and calculate a sine wave using this phase value. \n 
\begin{figure}[ht]
	\centering
  \includegraphics[width=0.9\textwidth]{unityFreq.png}
	\caption{C\# code to generate sine wave sounds in Unity}
	\label{unityFreq}
\end{figure}

\section{SuperCollider}
When using SuperCollider as a sound engine, we need to start a SuperCollider server separated from the graphical component / application. Because of this, it's mandatory to somehow communicate between our Unity application and SuperCollider. For this we used the Open Sound Protocol (OSC) which was also evaluated by Vu Nguyen in his previous research project \cite{vu}. Our idea was to use a two way communication between SuperCollider and Unity because on the one hand, we want to tell SuperCollider which frequencies it should use for sound generation and on the other hand we want to send the actual frequency spectrum of the generated sounds back to Unity. The graphic \ref{fig2} illustrates the mentioned communication channels. \n As Unity does not come with it's own implementation of the OSC-protocol, we used a software called OSCJack for this purpose \cite{oscJack}.
We defined 4 different endpoints for that. 3 are for sending data to SuperCollider and one is to send frequency-data back to Unity. 
When we want to play notes like on a piano, we need to define a key on and a key off event. As we want to support several notes played at once, we need information about the index of the touch point which is pressed or released and we need information about the frequency which should be played. The endpoints we created are: \n 
\begin{enumerate}
  \item ``address: /keyOn | variable types: float, int'' \n 
  
Where the first parameter defines the frequency to play and the second is the index of the touch point / finger which was used to play the frequency.
  \item ``address: /keyOff | variable types: float, int'' \n
 
Where the first parameter defines the frequency which we want to stop and the second is the index of the touch point / finger which was actually released.
\end{enumerate}

Also we need an additional endpoint for setting a value for the pitch-bending. We need the actual strength of the pitch-bending effect as well as the index of the frequency / note which should be affected by this: 
\begin{enumerate}
  \item ``address: /vibratoX | variable types: float, int'' \n 
  
Where the first parameter defines the amount strength of the vibrato effect and the second is the index of the touch point / finger which was used to play the frequency we want to change.
\end{enumerate}



In order to receive frequency data from SuperCollider, we defined a fourth endpoint which send over the frequency data serialized as a string. 
\begin{enumerate}
  \item ``address: /freq | variable types: string'' \n 
  
Where the string parameter includes the serialized FFT data array.
\end{enumerate}

\begin{figure}[ht]
	\centering
  \includegraphics[width=0.9\textwidth]{unitySC.png}
	\caption{Communication between SuperCollider and Unity}
	\label{fig2}
\end{figure}

\section{AudioKit}
``AudioKit is an audio synthesis, processing, and analysis platform for iOS, macOS (including Catalyst), and tvOS'' \cite{audiokit}. We tried to evaluate this library for audio synthesis on iOS devices. While evaluating, we faced several problems: 
\begin{itemize}
    \item Stronger knowledge in C Marshalling is needed for communication 
    \item Long debug time (build Unity project, build XCode project and test on real iOS device / simulator)
\end{itemize}
Also it's important to mention that AudioKit is only available for iOS devices at the moment which is also another downside of the project. Neverless AudioKit is a powerful audio library for iOS which was also used for the well known synthesizer Synth One \cite{synthOne}. 

We added the AudioKit project to our XCode project using a podfile \cite{podfile}. Afterwards we created a so called bridge which is used to communicate between Unity's C\# code and AudioKit's Swift code (namely UnityPluginBridge.mm and UnityPlugin.swift). This allows us to call a C\# method in Unity which internally calls a swift method which will create a sound using the AudioKit library \cite{bridging}. With this method we were able to produce proper sounds when clicking on our UI grid. We were not yet able to receive the FFT data back from AudioKit or creating a vibrato effect for our generated sounds. For this, we need to get a better understanding of C Marshalling to serialize the FFT data properly when sending them from AudioKit to Unity. 


\chapter{Graphical part | Unity}
\section{Grid / Input}
We came up with the idea to create a grid with an adjustable size where each row represents one (pseudo)-octave. The columns are the different octaves one can play and each row represents a certain note. Furthermore we want to give the user an orientation about notes which sound good together. We decided to implemente a functionality which allows the user to select different columns / a scale (5, pentatonic or 7 heptatonic) which sound good together. The image \ref{grid1} shows a grid with 12 notes per octave (like our note system in western music) and 10 octaves. The image \ref{grid2} shows a grid with 16 notes per octave and 8 octaves. In both images the purple columns are the colums of a custom scale the user has chosen.  \n
We also wanted to visualize the generated sounds to give the musician an orientation about the musical piece he's creating. For this we visualized the frequency spectrum of the currently played sounds. In the image \ref{inst1}



\begin{figure}[ht]
	\centering
  \includegraphics[width=0.4\textwidth]{grid1.png}
	\caption{Instrument grid 12 notes}
	\label{grid1}
\end{figure}

\begin{figure}[ht]
	\centering
  \includegraphics[width=0.4\textwidth]{grid2.png}
	\caption{Instrument grid 16 notes}
	\label{grid2}
\end{figure}

\begin{figure}[ht]
	\centering
  \includegraphics[width=0.4\textwidth]{instrument.png}
	\caption{Instrument with visualized frequency spectrum}
	\label{inst1}
\end{figure}

\chapter{Limitations}
While evaluating several ways for sound synthesis, we faced different limitations for each way. When using AudioKit we only can support Apple devices which runs iOS. Furthermore we were not yet able to receive FFT data produces by AudioKit and visualize this in Unity. To do so, we need a deeper understanding of C Marshalling to properly serialize the FFT data and send them over to Unity. \n 

When using SuperCollider, we need another desktop computer beside our mobile device because SuperCollider runs on desktop computers. That means when using the instrument UI on a smartphone, we need another computer and establish a connection through the (local) network to send data between the SuperCollider server and our mobile device. \n 

When using Unity for sound generation, we were only able to produce a simple sine wave sound. The reason for this is that Unity only offers a way to manipulate the audio buffer directly and has no built in audio library for creating more sophisticated sounds / signals. It would be necessary to create our own audio syntehsis library which needs a lot of time and requires a deeper understand of sound synthesis. \n


\chapter{Conclusion and perspective}
In our project we evaluated different ways for sound synthesis and used them along with Unity. We showed that each of the evaluated ways has some downside but is somehow useful when creating a digital, microtonal instrument. Furthermore we developed an user interface including a note grid shader for our digital instrument and proposed a way for calculating dynnamically adjustable, microtonal frequencies and sounds which will be visualized by using the FFT data of the synthesized sounds. We were also able to run the project on mobile devices with the operating system iOS (iPhone 7, iPhone X) which shows that it can be used on mobile devices and visualize the sounds in realtime to explore and compose microtonal music.    \n 
There are more promising ways for doing sound synthesis with Unity. Matt Tytel created a plugin called AudioHelm TODO: reference which uses his synthesizer Helm to create sounds using Unity, it also works great on mobile devices. Also there is the Faust programming language which allows DSP and sound synthesis. Faust offers the possiblity to export code to make it usable with Unity which could also lead to new possibilities. Also when specializing on iOS devices, the AudioKit library is quiet powerful but we have not yet found a way to send the FFT data from Swift code to C\# code, even if we're sure it's possible, we would need to get a better understanding of C marshalling to add this functionality. \n

As Unity offers a lot of options for even 3D graphic programming, we could also imagine more options for visualizing the generated sounds. We could for example use a technique called raymarching to create 3 dimensional shapes and fractals which will then react to the frequency spectrum of the generated sound. \n  

\clearpage
%Anhang
\pagenumbering{Alph}

%Abbildungsverzeichnis
\listoffigures \clearpage

%Literaturverzeichnisse (getrennt nach Stichwort)
\bibliographystyle{ieeetr}
\printbibliography[heading=bibintoc, title={References}]\clearpage
\printbibliography[heading=bibintoc, keyword={image}, title={Image references}]\clearpage

\end{document}