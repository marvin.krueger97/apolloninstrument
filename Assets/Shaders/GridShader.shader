﻿//Created by Marvin Krueger
//This file was created by us and is used to generate a note grid with colored tiles.
Shader "Unlit/GridShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _numTones ("num of notes", int) = 12
        _numOctaves ("num of octaves", int) = 10
        _hepScale ("is heptatonic scale", int) = 0
        _pentScale ("is pentatonic scale", int) = 0
        _scale1 ("first note in scale", int) = 0
        _scale2 ("second note in scale", int) = 0
        _scale3 ("third note in scale", int) = 0
        _scale4 ("4th note in scale", int) = 0
        _scale5 ("5th note in scale", int) = 0
        _scale6 ("6th note in scale", int) = 0
        _scale7 ("7th note in scale", int) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            uniform float _numTones;
            uniform float _numOctaves;
            uniform int _hepScale, _pentScale;
            uniform int _scale1, _scale2, _scale3, _scale4, _scale5, _scale6, _scale7;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float grid(float2 st, float res)
            {
                float2 grid = frac(st*res);
                return (step(res,grid.x) * step(res,grid.y));
            }
 
            void main()
            {
            }

            float2 curTile(float2 uv, float cellWidth, float cellHeight){
                return float2((int)(uv.x*1000 / cellWidth), (int)(uv.y*1000 / cellHeight));
            }
            

            fixed4 frag (v2f i) : SV_Target
            {
                float numCells = _numTones; 
                float numCellsHeight = _numOctaves; 
                float cellWidth = 1000 / numCells;
                float cellHeight = 1000 / numCellsHeight;
                float lineThickness = 5;
                int curCell = (int)(sin(_Time.y)*numCells/2+numCells/2);
                float4 colorizedTile = float4(0,0,0,1); 
                float2 curTileIndex = curTile(i.uv, cellWidth, cellHeight); 

                if(tex2D(_MainTex, float2(curTileIndex.x/numCells+1/numCells*0.5, curTileIndex.y/numCellsHeight+1/numCellsHeight*0.5)).x == 1){
                    colorizedTile = float4(0,0,1,1); 
                }
                if(curTileIndex.x == 0 && curTileIndex.y == int(_numOctaves/2)){
                    // Base tone / Kammerton / A / Midi 69
                    colorizedTile += float4(1,0,0,1);
                } else if(_pentScale == 1 && (_scale1 == curTileIndex.x || _scale2 == curTileIndex.x || _scale3 == curTileIndex.x || _scale4 == curTileIndex.x || _scale5 == curTileIndex.x)){
                    colorizedTile += float4(0,0.5,0,1);
                } else if(_hepScale == 1 && (_scale1 == curTileIndex.x || _scale2 == curTileIndex.x || _scale3 == curTileIndex.x || _scale4 == curTileIndex.x || _scale5 == curTileIndex.x|| _scale6 == curTileIndex.x|| _scale7 == curTileIndex.x)){
                    colorizedTile += float4(0,0.5,0,1);
                }
                
                
                
                // Check for border
                if(i.uv.x*1000 > cellWidth && (i.uv.x*1000)%cellWidth < lineThickness){
                    return float4(0,0,0,0); 
                } else if(i.uv.y*1000 > cellHeight && (i.uv.y*1000)%cellHeight < lineThickness){
                    return float4(0,0,0,0); 
                } 
                else {
                    return float4(1,1,1,1) - colorizedTile;
                }
            }
            ENDCG
        }
    }
}
