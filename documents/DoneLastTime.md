Faust näher angeschaut —> mMn gut. 
Viele vorgefertigte Blöcke, aber soweit mir bekannt, auch anpassbar. 
Problem: Funktioniert nicht gut mit Unity. Kontakt mit Community / Github issues. Mittlerweile zumindest Ton in Unity, vieles funktioniert jedoch noch nicht wie gewünscht.
Recherche über Audioerzeugung in Unity.

Skales mit einbringen?
Harmonien: Tried to add e.g. Major Scale for 12 notes. We can also use this for 24 notes as used in arabic music. Any chance to find scales for octaves with other number of notes? 