﻿using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using OscJack;

public class PluginHelper : MonoBehaviour
{
    [SerializeField] private Text textResult;

    [DllImport("__Internal")]
    private static extern int _addTwoNumberInIOS(int a, int b);

    [DllImport("__Internal")]
    private static extern void _keyOn(float frequency, int midiNote);


    [DllImport("__Internal")]
    private static extern void _setParams(int waveForm, double att, double dec, double sus, double rel);

    [DllImport("__Internal")]
    private static extern void _keyOff(int midiNote);

    [DllImport("__Internal")]
    private static extern string _getFFT();

    [DllImport("__Internal")]
    private static extern void _keyOnSimple(float midiNote);

    public Texture2D texture;

    public Slider attack, decay, sustain, release;

    public int numberOfNotes, numberOfOcatves;
    public float intervalMultiplier;
    public float startFrequency;
    OscServer server;
    OscClient client;

    Texture2D spectrum;
    RenderTexture prevTex, targetTex;
    public static readonly string superColliderClientName = "SuperCollider";

    public bool scalingMode = false;
    List<string> m_DropOptions = new List<string> { "Strings", "Piano"};

    public void connectToSC(){
         server = new OscServer(9001); // Port number

        server.MessageDispatcher.AddCallback(
            "/freq",
            (string address, OscDataHandle data) => {
                freqData = parseArrayFromString(data.GetElementAsString(0));
            }
        );
        string address = this.GetComponent<ConfigManager>().settingsObj["sections"]["SuperCollider"]["IP"]["current"];
        client = new OscClient(address.Split(':')[0], int.Parse(address.Split(':')[1]));
    }

    void Start()
    {
        notesInScale = new List<int>(); 
        texture = new Texture2D(10, 10);
        GameObject.Find("Plane").GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
        currentlyPlayingNotes = new List<List<int>>();
        try{
            AddTwoNumber();
        } catch{
            
        }
        createFreqListener();
        spectrum = new Texture2D(256, 256);
         Color fillColor = Color.black;
        Color[] fillColorArray =  spectrum.GetPixels();
 
        for(var i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = fillColor;
        }
        
        spectrum.SetPixels( fillColorArray );
        
        spectrum.Apply();

        Shader shader = Shader.Find("Unlit/SpectrumTime");
        spectrumShader = new Material(shader);
        spectrumShader.hideFlags = HideFlags.HideAndDontSave;
        prevTex = new RenderTexture(512,512, 0, RenderTextureFormat.ARGB32);
        targetTex = new RenderTexture(512,512, 0, RenderTextureFormat.ARGB32);
        output.texture = targetTex;
    }

    private void OnDisable() {
        server.Dispose();
        client.Dispose();
    }

    public float[] freqData;

    public GameObject frequencies;

    public int port;


    public void createFreqListener(){
        freqData = new float[256];
        for(int i = 0; i < 256; i++){
            freqData[i] = 0;
        }
    }

    float[] parseArrayFromString(string arr){
        float[] arrParsed = new float[256];
        string[] splitted = arr.Replace("[", "").Replace("]", "").Split(',');
        for(int i = 0; i < splitted.Length-1; i++){
            arrParsed[i] = float.Parse(splitted[i]);
        }

        return arrParsed;
    }
    public void sendOSCMessage(string subAddress, float msg)
    {
       client.Send(subAddress,       // OSC address
                msg); // Second element
    }

    public void sendOSCMessage(string subAddress, float msg, int index)
    {
       client.Send(subAddress,       // OSC address
                msg, index); // Second element
    }

    public void AddTwoNumber()
    {
        int result = _addTwoNumberInIOS(10, 5);
        Debug.Log("10 + 5  is : " + result);
    }

    List<List<int>> currentlyPlayingNotes;

    public void setParamsVals(){
        //_setParams(int.Parse(waveForm.text), attack.value, decay.value, sustain.value, release.value);
    }

    public void keyOnSC(float note, int index){
        if(index < 0){
            index = -index;
        }
        
        sendOSCMessage("/midi", note, index);
    }

    public void pitchChangeOSC(float pitch, float pitch2, int index){
         if(index < 0){
            index = -index;
        }
        sendOSCMessage("/vibratoX", pitch, index);
        sendOSCMessage("/vibratoY", pitch2, index);
    }

     public void pitchChangeInternal(float pitch, int index){
         if(index < 0){
            index = -index;
        }
        this.GetComponent<UnitySoundGen>().changePitch(index, pitch);
    }

    public void instrumentChanged(){
       // sendOSCMessage("/instrument", dropdown.value+1);
    }

    public void keyOffSC(float note, int index){
        if(index < 0){
            index = -index;
        }

        
        sendOSCMessage("/releasemidi", note, index);
    }

    public bool useSC;
    public bool useInternalSoundGeneration;

    public Material spectrumShader;
    public RawImage output;
    public int visual;

    List<int> notesInScale;

    void Update() {
        Color col = new Color(0,0,0,1);
        if(!useSC && !useInternalSoundGeneration){
           /*string fft =_getFFT(); 
           string[] vals = fft.Replace("[", "").Replace("]", "").Split(',');
            Debug.Log(vals[0]);
            for(int i = 0; i < 10; i++){
                if(vals.Length > i){
                    freqData[i] = float.Parse(vals[i]);
                }
            }*/
        }
        if(!useInternalSoundGeneration){
            for(int i = 0; i < 128; i++){
                frequencies.transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = new Vector2(4, freqData[i]*15f);
                col.r = (freqData[i]*30f) / 100f;
                spectrum.SetPixel(i*2, 0, col);
                spectrum.SetPixel(i*2+1, 0, col);

                spectrum.SetPixel(i*2, 1, col);
                spectrum.SetPixel(i*2+1, 1, col);
            }
            Graphics.CopyTexture(targetTex, prevTex);
            spectrum.Apply(); 
            spectrumShader.SetTexture("_spectrum", spectrum);
            spectrumShader.SetTexture("_prevTex", prevTex);
            spectrumShader.SetInt("_visual", visual);
            Graphics.Blit(null, targetTex, spectrumShader, 0);
        } else {
            float[] spectrumFFT = new float[512];
            this.GetComponent<AudioSource>().GetSpectrumData(spectrumFFT, 0, FFTWindow.BlackmanHarris);
            for(int i = 0; i < 128; i++){
                frequencies.transform.GetChild(i).GetComponent<RectTransform>().sizeDelta = new Vector2(4, spectrumFFT[i]*1500f);
                col.r = (spectrumFFT[i]*1500f) / 100f;
                spectrum.SetPixel(i*2, 0, col);
                spectrum.SetPixel(i*2+1, 0, col);

                spectrum.SetPixel(i*2, 1, col);
                spectrum.SetPixel(i*2+1, 1, col);
            }
            Graphics.CopyTexture(targetTex, prevTex);
            spectrum.Apply(); 
            spectrumShader.SetTexture("_spectrum", spectrum);
            spectrumShader.SetTexture("_prevTex", prevTex);
            spectrumShader.SetInt("_visual", visual);
            Graphics.Blit(null, targetTex, spectrumShader, 0);
        }

        var fingers = Lean.Touch.LeanTouch.Fingers;
        texture = new Texture2D(numberOfNotes, numberOfOcatves);
        texture.Apply();
        List<List<int>> notesPlayedBefore = currentlyPlayingNotes;
        currentlyPlayingNotes = new List<List<int>>();
        GameObject.Find("Plane").GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
        foreach (Lean.Touch.LeanFinger item in fingers)
        {
            if(!item.Up){
                Ray ray = Camera.main.ScreenPointToRay(item.ScreenPosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {
                    int midiNote = 21 + (int)(hit.textureCoord.y*numberOfOcatves)*numberOfNotes+(int)(hit.textureCoord.x*numberOfNotes);
                    int pitchChange = (int)(((decimal)(hit.textureCoord.x *numberOfNotes) % 1) * 100) + (int)(((decimal)(hit.textureCoord.y *numberOfOcatves) % 1) * 100) - 100;
                    if(useSC){
                        double y = (((double)(hit.textureCoord.y *numberOfOcatves) ));
                        y = y - Math.Truncate(y);
                        pitchChangeOSC((int)(((decimal)(hit.textureCoord.x *numberOfNotes) % 1)), (float)y, item.Index);
                    } else {
                        double y = (((double)(hit.textureCoord.y *numberOfOcatves) ));
                        y = y - Math.Truncate(y);

                        pitchChangeInternal((float)y, midiNote);
                    }

                    var decPlaces = (int)(((decimal)(hit.textureCoord.x *numberOfNotes) % 1) * 100);
                    bool isPlaying = false; 
                    foreach (var playingNote in currentlyPlayingNotes)
                    {
                        if(playingNote[0] == midiNote){
                            isPlaying = true;
                        }
                    }
                    if(!isPlaying){
                        currentlyPlayingNotes.Add(new List<int>{midiNote, item.Index});
                    }
                    
                    texture.SetPixel((int)(hit.textureCoord.x*numberOfNotes), (int)(hit.textureCoord.y*numberOfOcatves), Color.red);
                }
            }
        }
        foreach (List<int> item in notesPlayedBefore)
        {
            bool isStillPlaying = false; 
            foreach (var playingNote in currentlyPlayingNotes)
            {
                if(playingNote[0] == item[0]){
                    isStillPlaying = true;
                }
            }
            if(!isStillPlaying){
                int midiNote = item[0];
                Debug.Log("Off" + midiNote.ToString());
                int noteDistance = midiNote-69; // 69 is the Kammerton / start frequency --> https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
                float freq = startFrequency * UnityEngine.Mathf.Pow(intervalMultiplier, (float)noteDistance/(float)numberOfNotes); // Calculate frequency as described here --> http://techlib.com/reference/musical_note_frequencies.htm
                if(useSC){
                    keyOffSC(freq, item[1]);
                } else if(!useInternalSoundGeneration){
                    _keyOff(midiNote);
                } else {
                    this.GetComponent<UnitySoundGen>().onKeyOff(midiNote);
                    //this.GetComponent<UnitySoundGen>().frequency1 = 0;
                }
            }
            if(!currentlyPlayingNotes.Contains(item)){
                

            }
        }
        foreach (Lean.Touch.LeanFinger item in fingers)
        {
            if(!item.Up){
                Ray ray = Camera.main.ScreenPointToRay(item.ScreenPosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100))
                {

                    var decPlaces = (int)(((decimal)(hit.textureCoord.x *numberOfNotes) % 1) * 100);
                    int midiNote = 21 + (int)(hit.textureCoord.y*numberOfOcatves)*numberOfNotes+(int)(hit.textureCoord.x*numberOfNotes);
                    bool wasPlayedBefore = false; 
                    foreach (var playingNote in notesPlayedBefore)
                    {
                        if(playingNote[0] == midiNote){
                            wasPlayedBefore = true;
                        }
                    }
                    if(!wasPlayedBefore){
                        Debug.Log("Play" + midiNote.ToString());
                        int noteDistance = midiNote-21 - (numberOfNotes*(int)(numberOfOcatves/2f)); // 69 is the Kammerton / start frequency --> https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
                        float freq = startFrequency * UnityEngine.Mathf.Pow(intervalMultiplier, (float)noteDistance/(float)numberOfNotes); // Calculate frequency as described here --> http://techlib.com/reference/musical_note_frequencies.htm
                        //float freq = startFrequency * UnityEngine.Mathf.Pow(( UnityEngine.Mathf.Pow((float)intervalMultiplier, (1f/(float)numberOfNotes))), (float)noteDistance); // Using the formula hessling suggested 
                        Debug.Log("Play frequency" + freq);
                        //freqText.text = "Freq:" + freq.ToString(); 
                        
                        if(useSC){
                            keyOnSC(freq, item.Index);
                        }else if(!useInternalSoundGeneration){
                            _keyOn(freq, midiNote);
                        } else {
                            this.GetComponent<UnitySoundGen>().OnKey(midiNote,freq);
                        } 

                        if(scalingMode){
                            Debug.Log("Scaling, add" + (int)(hit.textureCoord.x*numberOfNotes));
                            addToScale((int)(hit.textureCoord.x*numberOfNotes)); 
                        }
                    }
                }
            }
        }
        
        texture.Apply();
        GameObject.Find("Plane").GetComponent<Renderer>().material.SetTexture("_MainTex", texture);
        GameObject.Find("Plane").GetComponent<Renderer>().material.SetInt("_numTones", numberOfNotes);
        GameObject.Find("Plane").GetComponent<Renderer>().material.SetInt("_numOctaves", numberOfOcatves);


    }
    



    public void addToScale(int note){
        if(notesInScale.Contains(note)){
            notesInScale.Remove(note);
        } else {
            if(notesInScale.Count == this.GetComponent<ConfigManager>().settingsObj["sections"]["Grid"]["Scaling mode"]["current"]["value"]){
                notesInScale[notesInScale.Count-1] = note;
            } else {
                notesInScale.Add(note);
            }
        }

        for(int i = 0; i < 7; i++){
            GameObject.Find("Plane").GetComponent<Renderer>().material.SetInt("_scale" + (i+1).ToString(), -1);
        }
        for(int i = 0; i < notesInScale.Count; i++){
            GameObject.Find("Plane").GetComponent<Renderer>().material.SetInt("_scale" + (i+1).ToString(), notesInScale[i]);

        }
            GameObject.Find("Plane").GetComponent<Renderer>().material.SetInt("_hepScale", this.GetComponent<ConfigManager>().settingsObj["sections"]["Grid"]["Scaling mode"]["current"]["value"] == 7?1:0);
            GameObject.Find("Plane").GetComponent<Renderer>().material.SetInt("_pentScale", this.GetComponent<ConfigManager>().settingsObj["sections"]["Grid"]["Scaling mode"]["current"]["value"] == 5?1:0);
    }

    public void toggleScalingMode(){
        scalingMode = !scalingMode;
    }
}